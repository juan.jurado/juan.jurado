### Hi there 👋, I'm Juan Jurado - aka [JJ][linkedin]


![GitHub User's stars](https://img.shields.io/github/stars/juan-jurado?label=Start&style=social)
[![LinkedIn Badge](https://img.shields.io/badge/LinkedIn-Profile-informational?style=flat&logo=linkedin&logoColor=white&color=0D76A8)][linkedin]
[![Twitter Badge](https://img.shields.io/badge/Twitter-Profile-informational?style=flat&logo=twitter&logoColor=white&color=1CA2F1)][twitter]


  ---
  
## 💻 I'm an engineer, developer, and entrepreneur!

<div align="justify">
  I'm EE (Electrical Engineer🧠) from Colombia🇨🇴 with a strong background in machine learning and deep learning for AI solutions and robotics powered by embedded systems (ARM64) such as Jetson devices by NVIDIA. I have experience as a self-driving cars engineer and embedded systems engineer at Kiwibot.com🤖💙 (self-driving robots for food delivery). Background in computer vision, robotics, control, localization, path planning, sensor fusion, motion planning, CNNs/RNNs, and system integration using ROS I/ROS II. Also, hardware-software interfaces (I2C, UART, Analog/Digital signals, USB, RS232, CANBus).
  <br />
  <br />
  Linux lover.
  <br />
  I do not want to maintain the status quo, ¡Let's create something great!🦾

<br />
<br />
- 🔭 I'm currently working at Globant.com as an IoT Edge Engineer building AI applications based on real-time video analysis using Deepstream, CNNs, and Transformers.
<br />
- 🌱 I'm currently learning low-level code (C++14) to improve my current robotic skills.
<br />
- 👯 I'm looking to collaborate on open source projects based on robotics and autonomous machines (AI at the edge + Robotics).
<br />
- ⚡ Fun fact: I used to play guitar🎸 ,drums, and piano🎹 - still learning. I love discovering a lot of tech things. I love deep house and Viking music🎧 while I'm coding. I'm Pet-friendly (Father of two cats and two dogs).🐕🦮🐈🐈‍⬛ Also, I swim🏊, mountain bike🚵,, and travel around the world in my motorcycle.🏍️
</div>
<br />



### Let's connect!

[![Youtube-name](https://img.shields.io/badge/YouTube-FF0000?style=for-the-badge&logo=youtube&logoColor=white)][youtube]
[![Twitter-name](https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)][twitter]
[![Instagram-name](https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white)][instagram]
[![Medium-name](https://img.shields.io/badge/Medium-12100E?style=for-the-badge&logo=medium&logoColor=white)][medium]
[![LinkedIn-name](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)][linkedin]
[![Gmail-name](https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white)][gmail]

  ---
### Skills: Tech Stack

<br />

[<img align="left" alt="yolo" height="30px" src="https://img.favpng.com/5/13/4/yolo-object-detection-darknet-opencv-convolutional-neural-network-png-favpng-j7FKX339NgCi1RcH8Sbs5ecHS.jpg" />][yolo]
[<img align="left" alt="tensorflow" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Tensorflow_logo.svg/1200px-Tensorflow_logo.svg.png" />][tensorflow]
[<img align="left" alt="keras" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Keras_logo.svg/1024px-Keras_logo.svg.png" />][keras]
[<img align="left" alt="atlassian" height="30px" src="https://seeklogo.com/images/A/atlassian-logo-DF2FCF6E4D-seeklogo.com.png" />][atlassian]
[<img align="left" alt="deepstream" height="30px" src="https://d7umqicpi7263.cloudfront.net/img/product/da3af25f-a9f3-48c3-ace2-ce646a94f8cf/167fc368-c07f-4b8d-b5fd-90f2c3454662" />][deepstream]
[<img align="left" alt="github" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Octicons-mark-github.svg/1024px-Octicons-mark-github.svg.png" />][github]
[<img align="left" alt="Visual Studio Code" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/b/bb/Gitea_Logo.svg" />][gitea]
[<img align="left" alt="Visual Studio Code" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/1024px-Visual_Studio_Code_1.35_icon.svg.png" />][vscode]
[<img align="left" alt="Python" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/1024px-Python-logo-notext.svg.png" />][python]
[<img align="left" alt="C++" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/306px-ISO_C%2B%2B_Logo.svg.png" />][c]\

[<img align="left" alt="ROS" height="30px" src="https://camo.githubusercontent.com/8fbe45b3aa44949d5cb3ce7619f1e7bb3bea0630/68747470733a2f2f75706c6f61642e77696b696d656469612e6f72672f77696b6970656469612f636f6d6d6f6e732f622f62622f526f735f6c6f676f2e737667" />][ros]
[<img align="left" alt="ROS2" height="30px" src="https://avatars3.githubusercontent.com/u/3979232?s=400&v=4" />][ros2]
[<img align="left" alt="gazebo" height="30px" src="http://gazebosim.org/assets/gazebo_vert-af0a0ada204b42b6daca54e98766979e45e011ea22347ffe90580458476d26d6.png" />][gazebo]
[<img align="left" alt="rviz" height="30px" src="https://store.yonohub.com/wp-content/uploads/2020/06/rviz.svg" />][rviz]
[<img align="left" alt="opencv" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/3/32/OpenCV_Logo_with_text_svg_version.svg" />][opencv]
[<img align="left" alt="ffmpeg" height="20px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/FFmpeg-Logo.svg/1280px-FFmpeg-Logo.svg.png" />][ffmpeg]
[<img align="left" alt="docker" height="30px" src="https://camo.githubusercontent.com/0af9f441e28f0f6acee28ca34e9ad438fd291fa3/68747470733a2f2f75706c6f61642e77696b696d656469612e6f72672f77696b6970656469612f636f6d6d6f6e732f342f34652f446f636b65725f253238636f6e7461696e65725f656e67696e652532395f6c6f676f2e737667" />][docker]

<br />

  ---
### Platforms

<br />

[<img align="left" alt="nvidia" height="30px" src="https://miro.medium.com/max/700/1*46tz-SDJRkv16vXdz0TAWA.png" />][nvidia]
[<img align="left" alt="raspi" height="30px" src="https://cdn.worldvectorlogo.com/logos/raspberry-pi.svg" />][raspi]
[<img align="left" alt="realsense" height="30px" src="https://www.mavis-imaging.com/media/image/e9/1e/24/Intel_RealSense_Logo_Color.png" />][realsense]
[<img align="left" alt="balena" height="30px" src="https://www.balena.io/docs/img/logo.svg" />][balena]
[<img align="left" alt="AzureIoT" height="30px" src="https://erwinstaal.nl/images/azure-iot-hub-450.png" />][azureiot]


[realsense]: https://www.intel.la/content/www/xl/es/architecture-and-technology/realsense-overview.html
[peplink]: https://www.peplink.com/
[balena]: https://www.balena.io/
[nvidia]: https://www.nvidia.com/es-la/
[slamtec]: https://www.slamtec.com/en/
[raspi]: https://www.raspberrypi.org/
[azureiot]: https://azure.microsoft.com/en-us/overview/iot/#overview

<br />

  ---



<!-- ---------------------------------------------------------------------- -->
[youtube]: https://www.youtube.com/channel/UCV-iMJ7VnpGcIbytF-so8yw
[globant]: https://www.globant.com/
[twitter]: https://twitter.com/JuanJuradoP
[instagram]: https://www.instagram.com/juanjuradop/?hl=en
[medium]: https://juanjuradop.medium.com/
[linkedin]: https://www.linkedin.com/in/juanfjuradop/
[gmail]: mailto:juanjuradop@gmail.com
[kiwibot]: https://www.kiwibot.com/

[vscode]: https://code.visualstudio.com/
[python]: https://www.python.org/
[c]: https://es.wikipedia.org/wiki/C%2B%2B
[ros]: https://www.ros.org/
[ros2]: https://index.ros.org/doc/ros2/
[gazebo]: http://gazebosim.org/
[rviz]: http://wiki.ros.org/rviz
[opencv]: https://opencv.org/
[docker]: https://www.docker.com/
[github]: https://github.com/
[ffmpeg]: https://ffmpeg.org/
[atlassian]: https://www.atlassian.com
[deepstream]: https://developer.nvidia.com/deepstream-sdk
[gitea]: https://gitea.io/en-us/

[yolo]: https://pjreddie.com/darknet/yolo/
[keras]: https://keras.io/
[tensorflow]: https://www.tensorflow.org/
